FROM php:8.2-apache

RUN apt-get update && \
    apt-get install -y git unzip libpq-dev && \
    docker-php-ext-install pdo pdo_pgsql

WORKDIR /var/www/html/

COPY . .

RUN chown -R www-data:www-data /var/www/html && \
    a2enmod rewrite

# Wyłącz domyślną konfigurację dla DocumentRoot i zezwól na przekierowanie do index.php
RUN sed -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/public/' /etc/apache2/sites-available/000-default.conf
RUN echo "DirectoryIndex index.php" >> /etc/apache2/sites-available/000-default.conf

EXPOSE 80
